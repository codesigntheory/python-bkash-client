
Changelog
=========

0.2.6 (2021-09-14)
-------------------
* Fix: Refund API.

0.2.2 (2021-09-14)
-------------------

* Fix: Search response.

0.2.1 (2021-09-14)
-------------------

* Fix: Query response.

0.0.0 (2021-06-23)
------------------

* First release on PyPI.
